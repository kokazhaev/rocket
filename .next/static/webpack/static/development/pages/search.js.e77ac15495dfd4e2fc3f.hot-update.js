webpackHotUpdate("static/development/pages/search.js",{

/***/ "./node_modules/form-urlencoded/form-urlencoded.js":
false,

/***/ "./node_modules/lodash.get/index.js":
false,

/***/ "./node_modules/querystringify/index.js":
false,

/***/ "./node_modules/requires-port/index.js":
false,

/***/ "./node_modules/unsplash-js/lib/constants/index.js":
false,

/***/ "./node_modules/unsplash-js/lib/methods/auth.js":
false,

/***/ "./node_modules/unsplash-js/lib/methods/categories.js":
false,

/***/ "./node_modules/unsplash-js/lib/methods/collections.js":
false,

/***/ "./node_modules/unsplash-js/lib/methods/currentUser.js":
false,

/***/ "./node_modules/unsplash-js/lib/methods/photos.js":
false,

/***/ "./node_modules/unsplash-js/lib/methods/search.js":
false,

/***/ "./node_modules/unsplash-js/lib/methods/stats.js":
false,

/***/ "./node_modules/unsplash-js/lib/methods/users.js":
false,

/***/ "./node_modules/unsplash-js/lib/unsplash.js":
false,

/***/ "./node_modules/unsplash-js/lib/utils/index.js":
false,

/***/ "./node_modules/url-parse/index.js":
false,

/***/ "./pages/search/index.js":
/*!*******************************!*\
  !*** ./pages/search/index.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/kkdauren/Desktop/rocket/pages/search/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;

 // const unsplash = new Unsplash({
//   applicationId: "926994471783df134b4b3fb4a78100492d07759cf2ff3821dd04b9a466e1c1c1",
//   secret: "51f8a002fbaea89a918efeeda1c9b9fc3f48bafc7ffe5d26cf92b14fb145cba2"
// });

var Search = function Search(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(''),
      searchValue = _useState[0],
      setSearchValue = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({}),
      images = _useState2[0],
      setImages = _useState2[1];

  var searchImages = function searchImages() {
    unsplash.search.photos(searchValue, 1).then(function (res) {
      return res.json().then(function (json) {
        setImages(json.results);
      });
    });
  };

  var fullImagePage = function fullImagePage(id) {
    next_router__WEBPACK_IMPORTED_MODULE_1___default.a.push("/image?img=".concat(id));
  };

  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, __jsx("input", {
    type: "text",
    value: searchValue,
    onChange: function onChange(e) {
      return setSearchValue(e.target.value);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }), __jsx("button", {
    onClick: searchImages,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }, "search"), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }), __jsx("br", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }), images.length && images.map(function (item) {
    return __jsx("img", {
      src: item.urls.small,
      key: item.id,
      onClick: function onClick() {
        return fullImagePage(item.urls.full);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 34
      },
      __self: this
    });
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Search);

/***/ })

})
//# sourceMappingURL=search.js.e77ac15495dfd4e2fc3f.hot-update.js.map