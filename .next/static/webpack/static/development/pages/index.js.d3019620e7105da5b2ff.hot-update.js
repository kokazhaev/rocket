webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/Image/style.js":
/*!***********************************!*\
  !*** ./components/Image/style.js ***!
  \***********************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
var Img = {
  height: "300px",
  marginBottom: "20px",
  cursor: "pointer"
};
var Bg = {
  background: "rgba(255, 255, 255, 0.5)"
};
var styles = {
  Img: Img,
  Bg: Bg
};

/***/ })

})
//# sourceMappingURL=index.js.d3019620e7105da5b2ff.hot-update.js.map