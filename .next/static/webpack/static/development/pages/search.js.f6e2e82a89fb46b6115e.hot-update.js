webpackHotUpdate("static/development/pages/search.js",{

/***/ "./pages/search/styles.js":
/*!********************************!*\
  !*** ./pages/search/styles.js ***!
  \********************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
var Input = {
  width: "500px",
  height: "40px",
  borderRadius: "5px",
  outline: "none",
  border: "1px solid #eeeeef",
  boxSizing: "border-box",
  padding: "0 20px",
  fontSize: "14px",
  marginRight: "20px"
};
var Button = {
  width: "100px",
  height: "40px",
  borderRadius: "5px",
  border: "1px solid #eeeeef",
  cursor: "pointer"
}; // const ErrorMessage = {
//   color: "white",
//   fontSize: "13px"
// }

var styles = {
  Input: Input,
  Button: Button
};

/***/ })

})
//# sourceMappingURL=search.js.f6e2e82a89fb46b6115e.hot-update.js.map