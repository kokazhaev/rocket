webpackHotUpdate("static/development/pages/search.js",{

/***/ "./pages/search/styles.js":
/*!********************************!*\
  !*** ./pages/search/styles.js ***!
  \********************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
var Main = {};
var Head = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginBottom: "20px"
};
var Input = {
  width: "500px",
  height: "40px",
  borderRadius: "5px",
  outline: "none",
  border: "1px solid #eeeeef",
  boxSizing: "border-box",
  padding: "0 20px",
  fontSize: "14px",
  marginRight: "20px"
};
var Button = {
  width: "100px",
  height: "40px",
  borderRadius: "5px",
  border: "1px solid #eeeeef",
  cursor: "pointer",
  outline: "none"
};
var ImagesBlock = {
  width: "100%",
  display: "flex",
  flexFlow: "row wrap",
  justifyContent: "space-between"
};
var Image = {
  height: "300px",
  marginBottom: "20px",
  cursor: "pointer"
};
var styles = {
  Main: Main,
  Head: Head,
  Input: Input,
  Button: Button,
  ImagesBlock: ImagesBlock,
  Image: Image
};

/***/ })

})
//# sourceMappingURL=search.js.8b92a152074877211d74.hot-update.js.map