webpackHotUpdate("static/development/pages/search.js",{

/***/ "./pages/search/index.js":
/*!*******************************!*\
  !*** ./pages/search/index.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../api */ "./api.js");
/* harmony import */ var _styles_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./styles.js */ "./pages/search/styles.js");
var _jsxFileName = "/Users/kkdauren/Desktop/rocket/pages/search/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;





var Search = function Search(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(''),
      searchValue = _useState[0],
      setSearchValue = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({}),
      images = _useState2[0],
      setImages = _useState2[1];

  var searchImages = function searchImages() {
    _api__WEBPACK_IMPORTED_MODULE_2__["unsplash"].search.photos(searchValue, 1).then(function (res) {
      return res.json().then(function (json) {
        setImages(json.results);
      });
    });
  };

  var fullImagePage = function fullImagePage(url) {
    next_router__WEBPACK_IMPORTED_MODULE_1___default.a.push("/image?img=".concat(url));
  };

  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, __jsx("input", {
    style: _styles_js__WEBPACK_IMPORTED_MODULE_3__["styles"].Input,
    type: "text",
    value: searchValue,
    onChange: function onChange(e) {
      return setSearchValue(e.target.value);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }), __jsx("button", {
    onClick: searchImages,
    style: _styles_js__WEBPACK_IMPORTED_MODULE_3__["styles"].Button,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }, "search"), images.length && images.map(function (item) {
    return __jsx("img", {
      src: item.urls.small,
      key: item.id,
      onClick: function onClick() {
        return fullImagePage(item.urls.full);
      },
      __source: {
        fileName: _jsxFileName,
        lineNumber: 33
      },
      __self: this
    });
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (Search);

/***/ })

})
//# sourceMappingURL=search.js.b4074533505c70fb3d1b.hot-update.js.map