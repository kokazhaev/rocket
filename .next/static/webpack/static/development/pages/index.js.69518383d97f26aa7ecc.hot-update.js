webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/header/index.js":
/*!************************************!*\
  !*** ./components/header/index.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./components/header/styles.js");
var _jsxFileName = "/Users/kkdauren/Desktop/rocket/components/header/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


var Header = function Header(props) {
  return __jsx("div", {
    style: _styles__WEBPACK_IMPORTED_MODULE_1__["styles"].Header,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    __self: this
  }, __jsx("div", {
    style: _styles__WEBPACK_IMPORTED_MODULE_1__["styles"].Category,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, "\u0413\u043B\u0430\u0432\u043D\u0430\u044F"), __jsx("div", {
    style: _styles__WEBPACK_IMPORTED_MODULE_1__["styles"].Category,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }, "\u041F\u043E\u0438\u0441\u043A"), __jsx("div", {
    style: _styles__WEBPACK_IMPORTED_MODULE_1__["styles"].Category,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, "\u0418\u0441\u0442\u043E\u0440\u0438\u044F"));
};

/* harmony default export */ __webpack_exports__["default"] = (Header);

/***/ })

})
//# sourceMappingURL=index.js.69518383d97f26aa7ecc.hot-update.js.map