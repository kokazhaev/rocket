webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/header/styles.js":
/*!*************************************!*\
  !*** ./components/header/styles.js ***!
  \*************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
var Header = {
  height: "50px",
  borderBottom: "1px solid #eeeeef",
  display: "flex",
  alignItems: "center"
};
var Category = {
  marginRight: "40px",
  cursor: "pointer"
};
var styles = {
  Header: Header,
  Category: Category
};

/***/ })

})
//# sourceMappingURL=index.js.d7b576e3cd559982a935.hot-update.js.map