webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/Image/index.js":
/*!***********************************!*\
  !*** ./components/Image/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "./node_modules/next/dist/client/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./style */ "./components/Image/style.js");
var _jsxFileName = "/Users/kkdauren/Desktop/rocket/components/Image/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



var ImageBlock = function ImageBlock(props) {
  // переход на страницу с url на полную картинку
  var fullImagePage = function fullImagePage(url) {
    next_router__WEBPACK_IMPORTED_MODULE_1___default.a.push("/image?img=".concat(url));
  };

  return __jsx("div", {
    style: _style__WEBPACK_IMPORTED_MODULE_2__["styles"].Bg,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }, __jsx("img", {
    src: props.url,
    style: _style__WEBPACK_IMPORTED_MODULE_2__["styles"].Img,
    onClick: function onClick() {
      return fullImagePage(props.full);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (ImageBlock);

/***/ })

})
//# sourceMappingURL=index.js.f2a81e4b9cc04a7c1419.hot-update.js.map