webpackHotUpdate("static/development/pages/search.js",{

/***/ "./pages/search/index.js":
/*!*******************************!*\
  !*** ./pages/search/index.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../api */ "./api.js");
/* harmony import */ var _styles_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./styles.js */ "./pages/search/styles.js");
/* harmony import */ var _components_MainLayout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/MainLayout */ "./components/MainLayout/index.js");
/* harmony import */ var _components_Image__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../components/Image */ "./components/Image/index.js");
var _jsxFileName = "/Users/kkdauren/Desktop/rocket/pages/search/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






var Search = function Search(props) {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])(''),
      searchValue = _useState[0],
      setSearchValue = _useState[1];

  var _useState2 = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])({}),
      images = _useState2[0],
      setImages = _useState2[1]; // запрос для пойска картинок


  var searchImages = function searchImages() {
    _api__WEBPACK_IMPORTED_MODULE_1__["unsplash"].search.photos(searchValue, 1).then(function (res) {
      return res.json().then(function (json) {
        setImages(json.results);
        saveToHistory();
      });
    });
  }; // для сохр истории


  var saveToHistory = function saveToHistory() {
    var history = [];

    if (localStorage.getItem('history')) {
      var newHis = localStorage.getItem('history');
      history = [newHis, searchValue];
      localStorage.setItem('history', history);
    } else {
      history.push(searchValue);
      localStorage.setItem('history', history);
      console.log('yeaj');
    }
  };

  console.log('arr', localStorage.getItem('history'));
  return __jsx(_components_MainLayout__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 39
    },
    __self: this
  }, __jsx("div", {
    style: _styles_js__WEBPACK_IMPORTED_MODULE_2__["styles"].Main,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }, __jsx("div", {
    style: _styles_js__WEBPACK_IMPORTED_MODULE_2__["styles"].Head,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }, __jsx("input", {
    style: _styles_js__WEBPACK_IMPORTED_MODULE_2__["styles"].Input,
    type: "text",
    value: searchValue,
    onChange: function onChange(e) {
      return setSearchValue(e.target.value);
    },
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }), __jsx("button", {
    onClick: searchImages,
    style: _styles_js__WEBPACK_IMPORTED_MODULE_2__["styles"].Button,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }, "search")), __jsx("div", {
    style: _styles_js__WEBPACK_IMPORTED_MODULE_2__["styles"].ImagesBlock,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 51
    },
    __self: this
  }, images.length && images.map(function (item) {
    return __jsx(_components_Image__WEBPACK_IMPORTED_MODULE_4__["default"], {
      url: item.urls.small,
      key: item.id,
      full: item.urls.full,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 54
      },
      __self: this
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (Search);

/***/ })

})
//# sourceMappingURL=search.js.47aa8ebbc7020d889796.hot-update.js.map