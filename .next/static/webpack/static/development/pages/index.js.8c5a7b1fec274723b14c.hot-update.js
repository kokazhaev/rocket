webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/header/styles.js":
/*!*************************************!*\
  !*** ./components/header/styles.js ***!
  \*************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
var Header = {
  height: "50px",
  borderBottom: "1px solid black",
  display: "flex",
  alignItems: "center",
  marginBottom: "20px"
};
var Category = {
  marginRight: "40px",
  cursor: "pointer"
};
var styles = {
  Header: Header,
  Category: Category
};

/***/ })

})
//# sourceMappingURL=index.js.8c5a7b1fec274723b14c.hot-update.js.map