webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/Image/index.js":
/*!***********************************!*\
  !*** ./components/Image/index.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style */ "./components/Image/style.js");
/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_style__WEBPACK_IMPORTED_MODULE_1__);
var _jsxFileName = "/Users/kkdauren/Desktop/rocket/components/Image/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;


var ImageBlock = function ImageBlock(props) {
  return __jsx("img", {
    src: props.url,
    style: _style__WEBPACK_IMPORTED_MODULE_1__["styles"].Img,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    __self: this
  });
};

/* harmony default export */ __webpack_exports__["default"] = (ImageBlock);

/***/ }),

/***/ "./components/Image/style.js":
/*!***********************************!*\
  !*** ./components/Image/style.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var Img = {
  height: "300px",
  marginBottom: "20px",
  cursor: "pointer"
};

/***/ })

})
//# sourceMappingURL=index.js.f008a4a7d18cdd144b59.hot-update.js.map