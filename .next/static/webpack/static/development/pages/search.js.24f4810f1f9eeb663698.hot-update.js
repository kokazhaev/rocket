webpackHotUpdate("static/development/pages/search.js",{

/***/ "./pages/search/styles.js":
/*!********************************!*\
  !*** ./pages/search/styles.js ***!
  \********************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
var Input = {
  width: "500px",
  height: "40px",
  borderRadius: "5px",
  outline: "none",
  border: "1px solid #eeeeef",
  boxSizing: "border-box",
  padding: "0 20px",
  fontSize: "14px",
  marginRight: "20px"
}; // const Header = {
//   padding: "10px 20px",
//   textAlign: "center",
//   color: "white",
//   fontSize: "22px"
// }
// const ErrorMessage = {
//   color: "white",
//   fontSize: "13px"
// }

var styles = {
  Input: Input
};

/***/ })

})
//# sourceMappingURL=search.js.24f4810f1f9eeb663698.hot-update.js.map