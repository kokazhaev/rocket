webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/header/styles.js":
/*!*************************************!*\
  !*** ./components/header/styles.js ***!
  \*************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
var Header = {
  height: "50px",
  borderBottom: "1px solid #eeeeef",
  display: "flex",
  alignItems: "center"
};
var Category = {
  marginRight: "40px"
};
var styles = {
  Header: Header,
  Category: Category
};

/***/ })

})
//# sourceMappingURL=index.js.4f5d90300e1c4086e4b2.hot-update.js.map