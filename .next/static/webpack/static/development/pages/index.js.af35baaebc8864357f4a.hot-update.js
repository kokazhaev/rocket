webpackHotUpdate("static/development/pages/index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../api */ "./api.js");
/* harmony import */ var _components_MainLayout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/MainLayout */ "./components/MainLayout/index.js");
/* harmony import */ var _components_Image__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Image */ "./components/Image/index.js");
/* harmony import */ var _style__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./style */ "./pages/style.js");
var _jsxFileName = "/Users/kkdauren/Desktop/rocket/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;






var Main = function Main() {
  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_0__["useState"])([]),
      images = _useState[0],
      setImages = _useState[1]; // до рендера записать картинки в state


  Object(react__WEBPACK_IMPORTED_MODULE_0__["useEffect"])(function () {
    _api__WEBPACK_IMPORTED_MODULE_1__["unsplash"].photos.listPhotos(20, 45, "latest").then(function (res) {
      return res.json().then(function (json) {
        setImages(json);
      });
    });
  }, []);
  return __jsx("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  }, __jsx(_components_MainLayout__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 21
    },
    __self: this
  }, __jsx("div", {
    style: _style__WEBPACK_IMPORTED_MODULE_4__["styles"].Main,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: this
  }, images && images.map(function (item) {
    return __jsx(_components_Image__WEBPACK_IMPORTED_MODULE_3__["default"], {
      url: item.urls.small,
      key: item.id,
      full: item.urls.full,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 24
      },
      __self: this
    });
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (Main);

/***/ })

})
//# sourceMappingURL=index.js.af35baaebc8864357f4a.hot-update.js.map