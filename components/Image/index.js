import Router from 'next/router';

import { styles } from './style';

const ImageBlock = props => {
  // переход на страницу с url на полную картинку
  const fullImagePage = url => {
    Router.push(`/image?img=${url}`)
  }

  return(
    <img src={props.url} style={styles.Img} onClick={() => fullImagePage(props.full)} />
  )
}

export default ImageBlock;