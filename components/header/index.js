import { styles } from './styles';
import Link from 'next/link';

const Header = props => {
  return (
    <div style={styles.Header}>
      <Link href='/'>
        <div style={styles.Category}>Главная</div>
      </Link>
      <Link href='/search'>
        <div style={styles.Category}>Поиск</div>
      </Link>
      <Link href='/history'>
        <div style={styles.Category}>История поиска</div>
      </Link>
    </div>
  )
}

export default Header;