const Header = {
  height: "50px",
  borderBottom: "1px solid black",
  display: "flex",
  alignItems: "center",
  marginBottom: "20px"
}
const Category = {
  marginRight: "40px",
  cursor: "pointer"
}

export const styles = {
  Header: Header,
  Category: Category
}