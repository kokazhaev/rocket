import Header from '../header';

const MainLayout = props => {
  return (
    <React.Fragment>
      <Header />
      <div>{ props.children }</div>
    </React.Fragment>
  )
}

export default MainLayout;