const Main = {
  width: "100%",
  display: "flex",
  flexFlow: "row wrap",
  justifyContent: "space-between"
}

export const styles = {
  Main: Main
}