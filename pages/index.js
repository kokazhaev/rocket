import { useState, useEffect } from 'react';
import { unsplash } from '../api';

import MainLayout from '../components/MainLayout';
import ImageBlock from '../components/Image';
import { styles } from './style';

const Main = () => {
  const [images, setImages] = useState([]);
  // до рендера записать картинки в state
  useEffect(() => {
    unsplash.photos.listPhotos(20, 45, "latest").then(res => res.json()
      .then(json => {
        setImages(json)
      })
    )
  }, [])

  return (
    <div>
      <MainLayout>
        <div style={styles.Main}>
          {images && images.map(item => {
            return <ImageBlock url={item.urls.small} key={item.id} full={item.urls.full} />
          })}
        </div>
      </MainLayout>
    </div>
  )
}

export default Main;