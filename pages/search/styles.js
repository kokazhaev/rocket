const Main = {
  
}

const Head = {
  display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginBottom: "20px"
}

const Input = {
  width: "500px",
  height: "40px",
  borderRadius: "5px",
  outline: "none",
  border: "1px solid black",
  boxSizing: "border-box",
  padding: "0 20px",
  fontSize: "14px",
  marginRight: "20px"
}

const Button = {
  width: "100px",
  height: "40px",
  borderRadius: "5px",
  border: "1px solid black",
  cursor: "pointer",
  outline: "none"
}

const ImagesBlock = {
  width: "100%",
  display: "flex",
  flexFlow: "row wrap",
  justifyContent: "space-between"
}

const Image = {
  height: "300px",
  marginBottom: "20px",
  cursor: "pointer"
}

export const styles = {
  Main: Main,
  Head: Head,
  Input: Input,
  Button: Button,
  ImagesBlock: ImagesBlock,
  Image: Image
}