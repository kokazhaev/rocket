import { useState } from 'react';
import { unsplash } from '../../api';

import { styles } from './styles.js';

import MainLayout from '../../components/MainLayout';
import ImageBlock from '../../components/Image';

const Search = props => {
  const [searchValue, setSearchValue] = useState('');
  const [images, setImages] = useState({});

  // запрос для пойска картинок
  const searchImages = () => {
    unsplash.search.photos(searchValue, 1)
      .then(res => {
        return res.json().then(json => {
          setImages(json.results)
        })
      });
  }

  return (
    <MainLayout>
      <div style={styles.Main}>
        <div style={styles.Head}>
          {/*  */}
          <input 
            style={styles.Input}
            type="text" value={searchValue} onChange={(e) => setSearchValue(e.target.value)}
          />
          {/* button */}
          <button onClick={searchImages} style={styles.Button}>search</button>
        </div>
        
        <div style={styles.ImagesBlock}>
        {/* если есть пройтись и показать */}
        {images.length && images.map(item => {
          return <ImageBlock url={item.urls.small} key={item.id} full={item.urls.full} />
        })}
        </div>
      </div>
    </MainLayout>
  )
}

export default Search;